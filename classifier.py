import utils


class Classifier:
    def __init__(self):
        self.data = None
        self.res_attribute = None

    def learn(self, train_data, res_attribute):
        self.res_attribute = res_attribute

    def classify(self, test_data):
        result = []
        return result


class MetricClassifier(Classifier):
    def __init__(self, metric=utils.Metric.euclidean):
        Classifier.__init__(self)
        self.metric = metric

    def learn(self, train_data, res_attribute):
        Classifier.learn(self, train_data, res_attribute)
        self.data = train_data


class NearestNeighbourClassifier(MetricClassifier):
    def __init__(self, metric=utils.Metric.euclidean):
        MetricClassifier.__init__(self, metric)

    def classify(self, test_data):
        result = []
        for test_entity in test_data:
            best = {'dist': 1e15, 'res': ''}
            for train_entity in self.data:
                a = train_entity.copy()
                b = test_entity.copy()
                a.pop(self.res_attribute, None)
                b.pop(self.res_attribute, None)
                new_dist = self.metric(a, b)
                if new_dist < best['dist']:
                    best['dist'] = new_dist
                    best['res'] = train_entity[self.res_attribute]
            result.append(best['res'])
        return result


class KNearestNeighbourClassifier(MetricClassifier):
    def __init__(self, k=1, metric=utils.Metric.euclidean):
        MetricClassifier.__init__(self, metric)
        self.k = k

    def classify(self, test_data):
        result = []
        for test_entity in test_data:
            dist = []
            for train_entity in self.data:
                a = train_entity.copy()
                b = test_entity.copy()
                a.pop(self.res_attribute, None)
                b.pop(self.res_attribute, None)
                dist.append({'dist': self.metric(a, b),
                             'res': train_entity[self.res_attribute]})
            dist.sort(key=lambda x: x['dist'])
            dist = dist[:self.k]

            val = []
            for element in dist:
                val.append(element['res'])
            max_val = utils.Utils.most_frequent(val)

            result.append(max_val)
        return result


class LogicalClassifier(Classifier):
    def __init__(self):
        Classifier.__init__(self)

    def learn(self, train_data, res_attribute):
        Classifier.learn(self, train_data, res_attribute)
        self.data = LogicalClassifier.Node.learn_id3(train_data, res_attribute)

    def classify(self, test_data):
        return [self.data.find(x) for x in test_data]

    class Node:
        def __init__(self, type=None, attribute=None, value=None):
            self.attribute = attribute
            self.type = type
            self.value = value
            self.left = None
            self.right = None

        def find(self, element):
            if self.type is not None:
                return self.type
            divider = LogicalClassifier.Divider(utils.Dataset([element]), None)
            a, b = divider.split_data(self.attribute, self.value)
            if a.row_count() == 0:
                return self.right.find(element)
            else:
                return self.left.find(element)

        @staticmethod
        def learn_id3(data, res_attribute):
            values = set([x[res_attribute] for x in data])
            if len(values) == 1:
                return LogicalClassifier.Node(utils.Utils.most_frequent(values))

            divider = LogicalClassifier.Divider(data, res_attribute)
            attribute, value, res = divider.divide()
            if value is None:
                return LogicalClassifier.Node(utils.Utils.most_frequent(values))
            a, b = divider.split_data(attribute, value)
            if a.row_count() == 0 or b.row_count() == 0:
                return LogicalClassifier.Node(utils.Utils.most_frequent(values))

            node = LogicalClassifier.Node(attribute=attribute, value=value)
            node.left = LogicalClassifier.Node.learn_id3(a, res_attribute)
            node.right = LogicalClassifier.Node.learn_id3(b, res_attribute)
            return node

    class Divider:
        def __init__(self, data, res_attribute, divide_method=utils.Utils.d_measure):
            self.data = data
            self.res_attribute = res_attribute
            self.divide_method = divide_method
            self.div = None
            self.div_attribute = None
            self.div_value = None

        def divide(self):
            self.div = 0
            for attr in self.data.attributes():
                if attr == self.res_attribute:
                    continue
                values = set(x[attr] for x in self.data)
                last_value = None
                for value in values:
                    if last_value is not None:
                        mean = utils.Utils.mean([last_value, value])
                        a, b = self.split_data(attr, mean)
                        new_res = self.rate_divide(a, b)
                        if new_res > self.div:
                            self.div = new_res
                            self.div_attribute = attr
                            self.div_value = mean
                    last_value = value
            return self.div_attribute, self.div_value, self.div

        def split_data(self, attribute, value):
            a, b = [], []
            for el in self.data:
                if el[attribute] < value:
                    a.append(el)
                else:
                    b.append(el)
            return utils.Dataset(a), utils.Dataset(b)

        def rate_divide(self, a, b):
            a = [x[self.res_attribute] for x in a]
            b = [x[self.res_attribute] for x in b]
            return self.divide_method(a, b)
