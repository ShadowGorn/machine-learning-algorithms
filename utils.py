import math
import random
from collections import Counter


class Utils:
    @staticmethod
    def rescaling(data):
        min_val = min(data)
        max_val = max(data)
        diff = max_val - min_val
        return [(x - min_val) / diff for x in data]

    @staticmethod
    def mean(data):
        return sum(data) / len(data)

    @staticmethod
    def deviation(data):
        m = Utils.mean(data)
        deviations = [(x - m) * (x - m) for x in data]
        return math.sqrt(Utils.mean(deviations))

    @staticmethod
    def standartization(data):
        m = Utils.mean(data)
        d = Utils.deviation(data)
        return [(x - m) / d for x in data]

    @staticmethod
    def information(q):
        return -q * math.log(q, 2) - (1 - q) * math.log(1 - q, 2)

    @staticmethod
    def i_gain(n, p, len, P):
        return Utils.information(P / len) - (p + n) * Utils.information(p / (p + n)) / len - \
               (len - p - n) * Utils.information((P - p) / (len - P - n)) / len

    @staticmethod
    def boosting_measure(n, p):
        return math.sqrt(p) - math.sqrt(n)

    @staticmethod
    def jinny_impurity(a, b):
        a_split = Utils.equal_count(a)
        b_split = Utils.equal_count(b)
        length = len(a) + len(b)
        res = 0
        for x in a_split:
            freq = x / len(a)
            res += freq * (1 - freq)
        for x in b_split:
            freq = x / len(b)
            res += freq * (1 - freq)
        return res

    @staticmethod
    def d_measure(a, b):
        a_split = Utils.equal_count(a)
        b_split = Utils.equal_count(b)
        res = 0
        for x in a_split:
            res += a_split[x] * (len(b) - b_split.get(x, 0))
        for x in b_split:
            res += b_split[x] * (len(a) - a_split.get(x, 0))
        return res

    @staticmethod
    def equal_count(data):
        res = dict()
        for el in data:
            if el in res:
                res[el] += 1
            else:
                res[el] = 1
        return res

    @staticmethod
    def most_frequent(data):
        return Counter(data).most_common(1)[0][0]

    @staticmethod
    def split_data(data, train_part):
        a = []
        b = []
        random.shuffle(data)
        cnt = len(data) * train_part
        for x in data:
            if len(a) < cnt:
                a.append(x)
            else:
                b.append(x)
        return Dataset(a), Dataset(b)


class Metric:
    @staticmethod
    def euclidean(a, b):
        dist = 0
        for x in a:
            dist += (a[x] - b[x]) * (a[x] - b[x])
        return dist


class Weight:
    @staticmethod
    def unit(x):
        return 1


class Checker:
    @staticmethod
    def LOO(classifier, train_data, res_attribute):
        ans = 0
        popped = None
        for i in range(0, train_data.row_count()):
            if popped is not None:
                train_data.data.append(popped)
            popped = train_data.data.pop(0)
            classifier.learn(train_data, res_attribute)
            test_dataset = Dataset([popped])
            res = classifier.classify(test_dataset)
            if res[0] != popped[res_attribute]:
                ans += 1
        return ans


class Dataset:
    def __init__(self, csv_data):
        self.data = []
        self.index = 0
        for row in csv_data:
            new_entity = {}
            for e in row:
                new_entity[e] = int(row[e])
            self.data.append(new_entity)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, item):
        return self.data[item]

    def __setitem__(self, key, value):
        self.data[key] = value

    def row_count(self):
        return len(self.data)

    def attributes(self):
        return list(self.data[0])

    def remove_attribute(self, attribute):
        for x in self.data:
            x.pop(attribute, None)

    def __iter__(self):
        self.index = -1
        return self

    def __next__(self):
        self.index += 1
        if self.index == self.row_count():
            raise StopIteration
        return self.data[self.index]
