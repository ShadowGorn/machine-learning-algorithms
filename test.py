import csv
import sys

import utils
import classifier

train_data, test_data = utils.Utils.split_data(utils.Dataset(csv.DictReader(open('haberman.data'))), 0.3)
res_attribute = 'survival'
k = 3
ans = 0

cl = classifier.KNearestNeighbourClassifier(k)
# ans = utils.Checker.LOO(cl,train_data,res_attribute)
cl.learn(train_data, res_attribute)
res = cl.classify(test_data)

for x, y in zip(res, test_data):
    if x == y[res_attribute]:
        ans += 1

print(ans)
print(ans / test_data.row_count())
